﻿using Polenter.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameToKey
{
    public class Profile
    {
        public string Name { get; set; }

        public string GUID { get; set; }

        public string ProcessName { get; set; }

        public string DeviceName { get; set; }

        public int MouseSpeed { get; set; }

        public Dictionary<string, string> assignments { get; set; }

        public Profile()
        {

            assignments = new Dictionary<string, string>();
            MouseSpeed = 2;
        }


        public static void saveProfiles(List<Profile> Profiles)
        {
            var serializer = new SharpSerializer();
            List<Profile> savedProfiles = Profiles;

            serializer.Serialize(savedProfiles, System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GamteToKey", "Profiles.xml"));
        }

        public static List<Profile> loadProfiles()
        {
            
            // using default constructor serializes to xml
           var serializer = new SharpSerializer();
           List<Profile> savedProfiles;
            if (System.IO.File.Exists(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GamteToKey", "Profiles.xml")))
                try {
                    savedProfiles = (List<Profile>)serializer.Deserialize(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GamteToKey", "Profiles.xml"));
                }
                catch
                {
                    savedProfiles = new List<Profile>();
                }
            else
                savedProfiles = new List<Profile>();

            return savedProfiles;
        }


   

    }
}
