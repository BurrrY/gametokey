﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameToKey.src
{
    public class Def
    {

        public enum Buttons
        {
            POV_Up,
            POV_Right,
            POV_Down,
            POV_Left
        }


        public enum Axis
        {
            X = 0,
            Y = 4,
            Z = 8,
            RotationX = 12,
            RotationY = 16,
            RotationZ = 20,
        }

        public enum AxisMove
        {
            UpRight,
            DownLeft,
            Stop
        }
    }
}
