﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.DirectInput;

namespace GameToKey.src
{

    public delegate void MyEventDelegate(DeviceBtnEventArgs _args);
    public delegate void MyAxisEventDelegate(AxisEventArgs _args);


    class gtkInputDevice
    {
        Joystick device;
        Thread deviceThread;
        ThreadStart deviceThreadStart;
        private bool pollData = true;
        public bool[] buttonState;
        public bool[] pointOfViewState;

        public event MyAxisEventDelegate AxisMoved;
        public event MyEventDelegate ButtonPressed;
        public event EventHandler POVPressed;

        public gtkInputDevice(string p)
        {
            deviceThreadStart = new ThreadStart(getData);
            deviceThread = new Thread(deviceThreadStart);
            buttonState = new bool[32];
            pointOfViewState = new bool[4];

            connectTo(p);
        }

        private void connectTo(string p)
        {
            // Initialize DirectInput
            var directInput = new DirectInput();
            // Find a Joystick Guid
            var joystickGuid = Guid.Empty;

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                if(deviceInstance.ProductGuid.ToString() == p)
                    joystickGuid = deviceInstance.InstanceGuid;



            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
            {
                string devGUID = deviceInstance.ProductGuid.ToString();
                if (devGUID == p)
                    joystickGuid = deviceInstance.InstanceGuid;

            }

            // If Joystick not found, throws an error
            if (joystickGuid == Guid.Empty)
            {
               // updateText(tbData, "No joystick/Gamepad found.");
            }
            else
            {
                device = new Joystick(directInput, joystickGuid);


                // Set BufferSize in order to use buffered data.
                device.Properties.BufferSize = 128;

                // Acquire the joystick
                device.Acquire();

                //Start Polling
                if(deviceThread == null || deviceThread.ThreadState == ThreadState.Aborted)
                    deviceThread = new Thread(deviceThreadStart);

                deviceThread.Start();
            }
        }

        private void getData() {
            while (pollData)
            {                
                device.Poll();
                JoystickUpdate[] polledData = device.GetBufferedData();
                foreach (JoystickUpdate state in polledData)
                {
                    if (state.Offset == JoystickOffset.PointOfViewControllers0)
                    {
                        switch (state.Value)
                        {
                            case 0:
                                pointOfViewState[(int)Def.Buttons.POV_Up] = true;
                                break;
                            case 9000:
                                pointOfViewState[(int)Def.Buttons.POV_Right] = true;
                                break;
                            case 18000:
                                pointOfViewState[(int)Def.Buttons.POV_Down] = true;
                                break;
                            case 27000:
                                pointOfViewState[(int)Def.Buttons.POV_Left] = true;
                                break;


                            case 4500:
                                buttonState[(int)Def.Buttons.POV_Up] = true;
                                buttonState[(int)Def.Buttons.POV_Right] = true;
                                break;
                            case 13500:
                                buttonState[(int)Def.Buttons.POV_Right] = true;
                                buttonState[(int)Def.Buttons.POV_Down] = true;
                                break;
                            case 22500:
                                buttonState[(int)Def.Buttons.POV_Down] = true;
                                buttonState[(int)Def.Buttons.POV_Left] = true;
                                break;
                            case 31500:
                                buttonState[(int)Def.Buttons.POV_Left] = true;
                                buttonState[(int)Def.Buttons.POV_Up] = true;
                                break;
                            case -1:
                                pointOfViewState[(int)Def.Buttons.POV_Up] = false;
                                pointOfViewState[(int)Def.Buttons.POV_Right] = false;
                                pointOfViewState[(int)Def.Buttons.POV_Left] = false;
                                pointOfViewState[(int)Def.Buttons.POV_Down] = false;

                                break;
                            default:
                                //updateText(tbData, state.Offset.ToString() + "--" + state.Value.ToString());
                                break;
                        }
                        OnPOVPressed(EventArgs.Empty);
                    }
                    else if (state.Offset == JoystickOffset.X)
                    {
                        OnAxisMoved(new AxisEventArgs(Def.Axis.X, state.Value));
                    }
                    else if (state.Offset == JoystickOffset.Y)
                    {
                        OnAxisMoved(new AxisEventArgs(Def.Axis.Y, state.Value));
                    }
                    else if (state.Offset == JoystickOffset.Z)
                    {
                        OnAxisMoved(new AxisEventArgs(Def.Axis.Z, state.Value));
                    }
                    else if (state.Offset == JoystickOffset.RotationZ)
                    {
                        OnAxisMoved(new AxisEventArgs(Def.Axis.RotationZ, state.Value));
                    }
                    else
                    {
                        string src = state.Offset.ToString();
                        if (src.StartsWith("Buttons"))
                        {
                            int btnNum = Convert.ToInt32(src.Substring(7));
                            buttonState[btnNum] = state.Value > 20;
                            OnButtonPressed(new DeviceBtnEventArgs(btnNum, buttonState[btnNum]));
                        }
                    }
                }
                Thread.Sleep(1);
            }
        }

        private void OnAxisMoved(AxisEventArgs axisEventArgs)
        {
            MyAxisEventDelegate handler = AxisMoved;
            if (handler != null)
            {
                handler(axisEventArgs);
            }
        }

        private void OnPOVPressed(EventArgs e)
        {
            EventHandler handler = POVPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }

          protected virtual void OnButtonPressed(DeviceBtnEventArgs e)
        {
            MyEventDelegate handler = ButtonPressed;
            if (handler != null)
            {
                handler(e);
            }
        }


          internal void Stop()
          {
              pollData = false;
              deviceThread.Abort();
          }
    }



    public class AxisEventArgs : EventArgs
    {
        private Def.Axis _Axis;
        private int _Value;

        public AxisEventArgs(Def.Axis pAxis, int Value)
        {
            _Axis = pAxis;
            _Value = Value;
        }


        public Def.Axis Axis
        {
            get { return _Axis; }
        }

        public int Value
        {
            get { return _Value; }
        }
    }
    public class DeviceBtnEventArgs : EventArgs
    {
        private int btn;
        private bool stte;

        public DeviceBtnEventArgs(int Button, bool State)
        {
            btn = Button;
            stte = State;
        }


        public int Button
        {
            get { return btn; }
        }

        public bool State
        {
            get { return stte; }
        }
    }
}
