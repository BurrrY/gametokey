﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameToKey
{
    /// <summary>
    /// Interaktionslogik für frm_SelectProcess.xaml
    /// </summary>
    public partial class frm_SelectProcess : Window
    {
        private Profile selectedProfile;

        public frm_SelectProcess()
        {
            InitializeComponent();
        }

        public frm_SelectProcess(Profile selectedProfile)
        {
            InitializeComponent();
            this.selectedProfile = selectedProfile;

            loadData();
        }

        private void loadData()
        {
            lb_Inputs.Items.Clear();
            Process[] myProcesses = Process.GetProcesses();

            foreach (Process myProcess in myProcesses)
                lb_Inputs.Items.Add(myProcess.ProcessName);
            
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            if(lb_Inputs.SelectedItem != null)
                selectedProfile.ProcessName = lb_Inputs.SelectedItem.ToString();

            this.Close();
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_Update_Click(object sender, RoutedEventArgs e)
        {
            loadData();
        }
    }
}
