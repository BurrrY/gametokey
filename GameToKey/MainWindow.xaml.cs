﻿using GameToKey.src;
using Hardcodet.Wpf.TaskbarNotification;
using Polenter.Serialization;
using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InputManager;
using System.ComponentModel;

namespace GameToKey
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        gtkInputDevice inputDev;
        TaskbarIcon tbi = new TaskbarIcon();
        DataTable _inputData;
        Dictionary<string, Profile> profiles = new Dictionary<string, Profile>();
        Profile selectedProfile, activeProfile;

        const int axisOffset = 0;
        const int povOffset = 6;
        const int btnOffset = povOffset + 4;

        Brush greenBrush = new SolidColorBrush(Colors.Lime);
        Brush grayBrush = new SolidColorBrush(Colors.Gray);

        BackgroundWorker scanThread;
        bool runScanner = true;

        Thread mouseMoverThread;
        bool tick = false;
        bool mouseThreadRunning = true;
        int mouseMoveX = 0;
        int mouseMoveY = 0;
        int mouseMoveZ = 0;
        int mouseMoveRZ = 0;
        System.Windows.Controls.DataGridCell cell;


        public MainWindow()
        {
            InitializeComponent();

            tbi.Icon = Properties.Resources.gtkIcon;
            tbi.ToolTipText = "GameToKey";


            scanThread = new BackgroundWorker();
            scanThread.WorkerSupportsCancellation = true;
            scanThread.DoWork += scanThread_DoWork;


            ThreadStart mouseMoverThread_start = new ThreadStart(moveMouse);
            mouseMoverThread = new Thread(mouseMoverThread_start);
            mouseMoverThread.Start();

        }

        void scanThread_DoWork(object sender, DoWorkEventArgs e)
        {
            scan();
        }


        private void MainForm_Loaded(object sender, RoutedEventArgs e)
        {
            _inputData = new DataTable();
            _inputData.Columns.Add("Input", typeof(string));
            _inputData.Columns.Add("Alias", typeof(string));
            _inputData.Columns.Add("InputState", typeof(bool));
            _inputData.Columns.Add("Output", typeof(string));
            ioGrid.ItemsSource = _inputData.AsDataView();

            Array axes = Def.Axis.GetValues(typeof(Def.Axis));

            foreach(var v in axes)
            {
                var row = _inputData.NewRow();
                _inputData.Rows.Add(row);
                row["input"] = "Axis " + v.ToString();
                row["inputState"] = false;
                row["Output"] = "";
                
            }

            for (int i = 0; i < 4; ++i)
            {
                var row = _inputData.NewRow();
                _inputData.Rows.Add(row);
                row["input"] = "POV " + (i + 1).ToString();
                row["inputState"] = false;
                row["Output"] = "";
            }


            for (int i = 0; i < 32; ++i)
            {
                var row = _inputData.NewRow();
                _inputData.Rows.Add(row);
                row["input"] = "Button " + (i + 1).ToString();
                row["inputState"] = false;
                row["Output"] = "";
            }



            List<Profile> savedProfiles = Profile.loadProfiles();

            foreach (Profile p in savedProfiles)
                profiles.Add(p.Name, p);

            refreshProfiles();

            if (lb_Profiles.Items.Count > 0)
            {
                selectedProfile = profiles[lb_Profiles.Items.GetItemAt(0).ToString()];
                showProfile();
            }

        }

        private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (inputDev != null)
                inputDev.Stop();

            runScanner = false;
            scanThread.CancelAsync();

            mouseThreadRunning = false;
            mouseMoverThread.Abort();

            Profile.saveProfiles(profiles.Values.ToList<Profile>());
        }

        void dataCell_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (cb_useAssistent.IsChecked == false)
                return;

            cell = e.Source as System.Windows.Controls.DataGridCell;
            if (cell.Column.Header.ToString() != "Output")
                return;

            var currentRowIndex = ioGrid.Items.IndexOf(ioGrid.CurrentItem);

            btn_RemoveAssign.IsEnabled = true;
            TextBlock tb = cell.Content as TextBlock;
            if (currentRowIndex >= povOffset)
            {
                if (tb != null)
                    tb_KeyTest.Text = tb.Text;
                else
                    tb_KeyTest.Text = "";

                tb_KeyTest.IsEnabled = true;
                tb_KeyA.IsEnabled = false;
                tb_KeyB.IsEnabled = false;
                cb_Mouse.IsEnabled = false;
                btn_AssisOK.IsEnabled = false;
            }
            else
            {
                tb_KeyTest.IsEnabled = false;
                tb_KeyA.IsEnabled = true;
                tb_KeyB.IsEnabled = true;
                cb_Mouse.IsEnabled = true;
                btn_AssisOK.IsEnabled = true;
                if (tb.Text.Contains(','))
                {
                    tb_KeyA.Text = tb.Text.Split(',')[0];
                    tb_KeyB.Text = tb.Text.Split(',')[1];
                }
                else
                {
                    tb_KeyA.Text = "";
                    tb_KeyB.Text = "";
                    if (tb.Text == "MouseY")
                        cb_Mouse.SelectedIndex = 1;
                    else
                        cb_Mouse.SelectedIndex = 0;
                }
            }
        }

        private void tb_KeyTest_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            tb_KeyTest.Text = e.Key.ToString();
            e.Handled = true;
            cell.Content = tb_KeyTest.Text;
            cell = null;
            tb_KeyTest.IsEnabled = false;
            btn_RemoveAssign.IsEnabled = false;
            tb_KeyA.IsEnabled = false;
            tb_KeyB.IsEnabled = false;
            cb_Mouse.IsEnabled = false;
            btn_AssisOK.IsEnabled = false;
        }

        private void btn_RemoveAssign_Click(object sender, RoutedEventArgs e)
        {

            tb_KeyTest.Text = "";
            cell.Content = tb_KeyTest.Text;
            cell = null;
            tb_KeyTest.IsEnabled = false;
            btn_RemoveAssign.IsEnabled = false;
            btn_AssisOK.IsEnabled = false;
            tb_KeyA.IsEnabled = false;
            tb_KeyB.IsEnabled = false;
            cb_Mouse.IsEnabled = false;
        }


        private void cb_useAssistent_Click(object sender, RoutedEventArgs e)
        {
            ioGrid.IsReadOnly = (bool)cb_useAssistent.IsChecked;
        }

        private void btn_AssisOK_Click(object sender, RoutedEventArgs e)
        {

        }

        #region DeviceInput
        private void inputDev_POVPressed(object sender, EventArgs e)
        {
            //Display Stuff
            _inputData.Rows[(int)Def.Buttons.POV_Up + povOffset]["inputState"] = 
                inputDev.pointOfViewState[(int)Def.Buttons.POV_Up];

            _inputData.Rows[(int)Def.Buttons.POV_Left + povOffset]["inputState"] =
                inputDev.pointOfViewState[(int)Def.Buttons.POV_Left];

            _inputData.Rows[(int)Def.Buttons.POV_Right + povOffset]["inputState"] =
                inputDev.pointOfViewState[(int)Def.Buttons.POV_Right];

            _inputData.Rows[(int)Def.Buttons.POV_Down + povOffset]["inputState"] =
                inputDev.pointOfViewState[(int)Def.Buttons.POV_Down];



            inputAction((int)Def.Buttons.POV_Up + povOffset, inputDev.pointOfViewState[(int)Def.Buttons.POV_Up]);
            inputAction((int)Def.Buttons.POV_Left + povOffset, inputDev.pointOfViewState[(int)Def.Buttons.POV_Left]);
            inputAction((int)Def.Buttons.POV_Right + povOffset, inputDev.pointOfViewState[(int)Def.Buttons.POV_Right]);
            inputAction((int)Def.Buttons.POV_Down + povOffset, inputDev.pointOfViewState[(int)Def.Buttons.POV_Down]);
        }


        void inputDev_AxisMoved(AxisEventArgs _args)
        {
            string action = "none";
            int axisRowOffset = -1;


            Def.AxisMove direction = Def.AxisMove.Stop;

            if (_args.Axis == Def.Axis.X)
                axisRowOffset = 0;
            else if (_args.Axis == Def.Axis.Y)
                axisRowOffset = 1;
            else if (_args.Axis == Def.Axis.Z)
                axisRowOffset = 2;
            else if (_args.Axis == Def.Axis.RotationZ)
                axisRowOffset = 3;



            if (_args.Value > 65000)
                direction = Def.AxisMove.UpRight;
            else if (_args.Value < 10)
                direction = Def.AxisMove.DownLeft;
            else if (_args.Value < 33000 && _args.Value > 30000)
                direction = Def.AxisMove.Stop;
            else
                return;

            action = _inputData.Rows[axisRowOffset + axisOffset]["output"].ToString();
            _inputData.Rows[axisRowOffset + axisOffset]["Alias"] = _args.Value;
            if (action.Length == 0)
                return;
            

            if (action == "MouseX" || action == "MouseY" || action == "LeftRight" || action == "UpDown")
                axisAction(action, direction);
            else
            {
                string[] a = action.Split(',');
                
                if (a.Length != 2)
                    return;

             


                if (direction == Def.AxisMove.UpRight && _args.Axis == Def.Axis.X) // UP
                    keyboardPress(a[1], true);
                else if (direction == Def.AxisMove.UpRight && _args.Axis == Def.Axis.Y) // RIGHT
                    keyboardPress(a[1], true);
                else if (direction == Def.AxisMove.DownLeft && _args.Axis == Def.Axis.X) // DOWN
                    keyboardPress(a[0], true);
                else if (direction == Def.AxisMove.DownLeft && _args.Axis == Def.Axis.Y) // LEFT
                    keyboardPress(a[0], true);
                else if (direction == Def.AxisMove.Stop)
                {
                    keyboardPress(a[0], false);
                    keyboardPress(a[1], false);
                }
            }
        }

        void inputDev_ButtonPressed(DeviceBtnEventArgs _args)
        {
            int ID = _args.Button + btnOffset;
            _inputData.Rows[ID]["inputState"] = _args.State;
            inputAction(ID, _args.State);
        }

        #endregion


        #region outputPerform

        private void inputAction(int rowID, bool down)
        {
            string action;
            string key = (string)_inputData.Rows[rowID]["input"];

            if (!activeProfile.assignments.TryGetValue(key, out action))
                action = (string)_inputData.Rows[rowID]["output"];

            if (action.Length == 0)
                return;

            if (action == "LeftClick" || action == "RightClick")
            {
                sendMouseClick(action, down);
                return;
            }
            else if (action == "ScrollUp" || action == "ScrollDown")
            {
                sendMouseScroll(action);
                return;
            }
            else if ((action == "MouseLeft" || action == "MouseRight" || action == "MouseUp" || action == "MouseDown"))
            {
                if (down)
                    sendMouseMove(action);
                return;
            }
            else
            {
                keyboardPress(action, down);
            }
        }

        private void keyboardPress(string action, bool down)
        {
            try
            {
                Keys MyStatus = ParseEnum<Keys>(action);
                if (down)
                    InputManager.Keyboard.KeyDown(MyStatus);
                else
                    InputManager.Keyboard.KeyUp(MyStatus);
            }
            catch (Exception e)
            {
                tbi.ShowBalloonTip("KeyError", "Error on sending " + action, BalloonIcon.Error);
            }
        }

        private void sendMouseMove(string action)
        {
            const int step = 20;
            if (action == "MouseUp")
                InputManager.Mouse.MoveRelative(0, -step);
            else if (action == "MouseDown")
                InputManager.Mouse.MoveRelative(0, step);
            else if (action == "MouseLeft")
                InputManager.Mouse.MoveRelative(-step, 0);
            else if (action == "MouseRight")
                InputManager.Mouse.MoveRelative(step, 0);
        }

        private void sendMouseScroll(string direction)
        {
            if (direction == "ScrollUp")
                InputManager.Mouse.Scroll(InputManager.Mouse.ScrollDirection.Up);
            else if (direction == "ScrollDown")
                InputManager.Mouse.Scroll(InputManager.Mouse.ScrollDirection.Down);
            else
            {
                tbi.ShowBalloonTip("MouseError", "Error on " + direction, BalloonIcon.Error);
                return;
            }
        }

        private void sendMouseClick(string action, bool down)
        {
            InputManager.Mouse.MouseKeys Btn;

            if (action == "LeftClick")
                Btn = InputManager.Mouse.MouseKeys.Left;
            else if (action == "RightClick")
                Btn = InputManager.Mouse.MouseKeys.Right;
            else
            {
                tbi.ShowBalloonTip("MouseError", "Error on sending " + action, BalloonIcon.Error);
                return;
            }

            if (down)
                InputManager.Mouse.ButtonDown(Btn);
            else
                InputManager.Mouse.ButtonUp(Btn);
        }

        private void axisAction(string action, Def.AxisMove p)
        {
            if (action == "MouseX")
            {
                if (p == Def.AxisMove.UpRight)
                    mouseMoveX = selectedProfile.MouseSpeed;
                else if (p == Def.AxisMove.DownLeft)
                    mouseMoveX = -selectedProfile.MouseSpeed;
                else
                    mouseMoveX = 0;
            }
            else if (action == "MouseY")
            {
                if (p == Def.AxisMove.UpRight)
                    mouseMoveY = selectedProfile.MouseSpeed;
                else if (p == Def.AxisMove.DownLeft)
                    mouseMoveY = -selectedProfile.MouseSpeed;
                else
                    mouseMoveY = 0;
            }
            else if (action == "LeftRight")
            {
                if (p == Def.AxisMove.UpRight)
                    keyboardPress("Right", true);
                else if (p == Def.AxisMove.DownLeft)
                    keyboardPress("Left", true);
                else
                {
                    keyboardPress("Right", false);
                    keyboardPress("Left", false);
                }
            }
            else if (action == "UpDown")
            {
                if (p == Def.AxisMove.UpRight)
                    keyboardPress("Down", true);
                else if (p == Def.AxisMove.DownLeft)
                    keyboardPress("Up", true);
                else
                {
                    keyboardPress("Up", false);
                    keyboardPress("Down", false);
                }
            }
        }

        private void moveMouse()
        {
            while (mouseThreadRunning)
            {
                Win32.POINT p = new Win32.POINT();
                Win32.GetCursorPos(out p);
                p.x = p.x + mouseMoveX;
                p.y = p.y + mouseMoveY;

                Win32.SetCursorPos(p.x, p.y);
                Thread.Sleep(10);
            }
        }
        #endregion


        #region ScannerStuff

        public void scan()
        {
            while (runScanner)
            {
                if (activeProfile != null) //We've got something running
                {
                    if (processRunning(activeProfile.ProcessName) && deviceConnected(activeProfile.GUID))
                    {

                    }
                    else
                    {
                        disableProfile();
                    }
                }
                else
                {
                    foreach (var kvp in profiles)
                    {
                        Profile p = kvp.Value;
                        if (processRunning(p.ProcessName) && deviceConnected(p.GUID))
                        {
                            activateProfile(p);
                        }
                    }
                }

                scanThread.ReportProgress(1, tick);
                tick = !tick;

                Thread.Sleep(250);
            }
        }

        private bool deviceConnected(string p)
        {
            var directInput = new DirectInput();
            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                if (deviceInstance.ProductGuid.ToString() == p)
                    return true;

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                if (deviceInstance.ProductGuid.ToString() == p)
                    return true;

            return false;
        }

        private bool processRunning(string p)
        {
            Process[] myProcesses = Process.GetProcesses();
            foreach (Process myProcess in myProcesses)
                if (p == myProcess.ProcessName)
                    return true;

            return false;
        }

        #endregion


        #region GUIInput

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (selectedProfile == null)
                return;

            frm_SelectInput si = new frm_SelectInput(selectedProfile);
            si.ShowDialog();
            showProfile();
        }

        private void btn_NewProfile_Click(object sender, RoutedEventArgs e)
        {
            Profile p = new Profile();
            p.Name = "profile #" + profiles.Count.ToString();
            profiles.Add(p.Name, p);
            refreshProfiles();
        }

        private void lb_Profiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lb_Profiles.SelectedItem == null)
                return;

            selectedProfile = profiles[lb_Profiles.SelectedItem.ToString()];
            showProfile();
        }

        private void tb_ProfileName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (selectedProfile == null)
                return;

            selectedProfile.Name = tb_ProfileName.Text;

            refreshProfiles();
        }

        private void tb_Process_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void tb_InputDevice_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btn_SaveProfile_Click(object sender, RoutedEventArgs e)
        {
            if (selectedProfile == null)
                return;

            selectedProfile.assignments.Clear();

            foreach (DataRow row in _inputData.Rows)
            {
                string Key = row["Input"].ToString();
                string value = row["Output"].ToString();
                if (value == "-" || value.Length <= 0)
                    continue;

                selectedProfile.assignments.Add(Key, value);
            }


            Profile.saveProfiles(profiles.Values.ToList<Profile>());  

            List<Profile> savedProfiles = profiles.Values.ToList<Profile>();
            profiles.Clear();
            foreach (Profile p in savedProfiles)
                profiles.Add(p.Name, p);


            refreshProfiles();
        }


        private void btn_SelectProcess_Click(object sender, RoutedEventArgs e)
        {
            if (selectedProfile == null)
                return;

            frm_SelectProcess sp = new frm_SelectProcess(selectedProfile);
            sp.ShowDialog();
            showProfile();
        }


        private void cb_Automatic_Unchecked(object sender, RoutedEventArgs e)
        {
            runScanner = false;
            scanThread.CancelAsync();

            scanThread = null;
            lb_Profiles.IsEnabled = true;
            centerGrid.IsEnabled = true;
        }

        private void cb_Automatic_Checked(object sender, RoutedEventArgs e)
        {
            runScanner = true;

            if (scanThread == null)
                scanThread = new BackgroundWorker();

            scanThread.WorkerSupportsCancellation = true;
            scanThread.WorkerReportsProgress = true;
            scanThread.ProgressChanged += scanThread_ProgressChanged;
            scanThread.DoWork += scanThread_DoWork;
            scanThread.RunWorkerAsync();
            lb_Profiles.IsEnabled = false;
            centerGrid.IsEnabled = false;
        }
        private void cb_Active_Checked(object sender, RoutedEventArgs e)
        {
            if (selectedProfile == null)
                return;


            activateProfile(selectedProfile);

            cb_Automatic.IsEnabled = false;
            lb_Profiles.IsEnabled = false;
        }

        private void cb_Active_Unchecked(object sender, RoutedEventArgs e)
        {
            disableProfile();

            cb_Automatic.IsEnabled = true;
            lb_Profiles.IsEnabled = true;
        }

        private void tb_MouseSpeed_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (selectedProfile == null)
                return;

            selectedProfile.MouseSpeed = Convert.ToInt32(tb_MouseSpeed.Text);
        }

        #endregion

        
        private void showProfile()
        {
            tb_InputDevice.Text = selectedProfile.DeviceName;
            tb_Process.Text = selectedProfile.ProcessName;
            tb_ProfileName.Text = selectedProfile.Name;
            la_GUID.Content = selectedProfile.GUID;
            tb_MouseSpeed.Text = selectedProfile.MouseSpeed.ToString();

            foreach (DataRow row in _inputData.Rows)
            {
                string Key = row["Input"].ToString();
                if (selectedProfile.assignments.ContainsKey(Key))
                    row["output"] = selectedProfile.assignments[Key];
                else
                    row["output"] = "";
            }
        }
        
        public void refreshProfiles()
        {
            lb_Profiles.Items.Clear();
            foreach (var kvp in profiles)
                lb_Profiles.Items.Add(kvp.Key);
        }

        public static T ParseEnum<T>(string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void disableProfile()
        {
            tbi.ShowBalloonTip("Stopping Profile", activeProfile.Name + " is now inactive.", BalloonIcon.Info);
            inputDev.Stop();
            activeProfile = null;
        }

        private void activateProfile(Profile p)
        {
            activeProfile = p;
            inputDev = new gtkInputDevice(p.GUID);
            inputDev.ButtonPressed += inputDev_ButtonPressed;
            inputDev.POVPressed += inputDev_POVPressed;
            inputDev.AxisMoved += inputDev_AxisMoved;


            tbi.ShowBalloonTip("Start Profile", p.Name + " is now active.", BalloonIcon.Info);
        }

        void scanThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if ((bool)e.UserState)
                    Rect1.Fill = grayBrush;
                else
                    Rect1.Fill = greenBrush;
        }

        private void tb_KeyTest_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

        }


        private void MainForm_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }



    }

    public class Win32
    {
        [DllImport("User32.Dll")]
        public static extern long SetCursorPos(int x, int y);

        [DllImport("User32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetCursorPos(out POINT lpPoint);

        [DllImport("User32.Dll")]
        public static extern bool ClientToScreen(IntPtr hWnd, ref POINT point);

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int x;
            public int y;
        }
    }
}
