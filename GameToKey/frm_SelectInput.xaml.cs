﻿using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameToKey
{
    /// <summary>
    /// Interaktionslogik für dev_SelectInput.xaml
    /// </summary>
    public partial class frm_SelectInput : Window
    {
        private Profile selectedProfile;

        public frm_SelectInput()
        {
            InitializeComponent();
        }

        public frm_SelectInput(Profile selectedProfile)
        {
            InitializeComponent();
            this.selectedProfile = selectedProfile;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadData();
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            if (lb_Inputs.SelectedItem != null)
            {
                selectedProfile.DeviceName = lb_Inputs.SelectedItem.ToString().Substring(0, 10);
                selectedProfile.GUID = getGUID(lb_Inputs.SelectedItem.ToString());
            }


            this.Close();
        }

        private string getGUID(string p)
        {
            // Initialize DirectInput
            var directInput = new DirectInput();
            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                if (deviceInstance.ProductName == p)
                    return deviceInstance.ProductGuid.ToString();

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                if (deviceInstance.ProductName == p)
                    return deviceInstance.ProductGuid.ToString();

            return "";
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_Update_Click(object sender, RoutedEventArgs e)
        {
            loadData();
        }

        private void loadData()
        {
            lb_Inputs.Items.Clear();

            var directInput = new DirectInput();

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                lb_Inputs.Items.Add(deviceInstance.ProductName);

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                lb_Inputs.Items.Add(deviceInstance.ProductName);
        }
    }
}
